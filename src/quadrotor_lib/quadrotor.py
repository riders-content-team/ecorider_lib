import rospy
import math
import time
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from std_srvs.srv import Trigger
from gazebo_msgs.srv import GetModelState
from quadrotor_description.srv import LaserSensor, CameraSensor

# What is the best way to provide Vector operators?
# Also where to place so game controller can use it as well
class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class Robot:
    def __init__(self, robot_name):
        rospy.init_node("quadrotor_controller")

        self.quadrotor_cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

        self.last_twist_cmd = Twist()
        self.last_twist_cmd.linear.x = 0
        self.last_twist_cmd.linear.y = 0
        self.last_twist_cmd.linear.z = 0
        self.last_twist_cmd.angular.x = 0
        self.last_twist_cmd.angular.y = 0
        self.last_twist_cmd.angular.z = 0

        self._init_services()
        self._init_game_controller()

        self.target_coordinates = []

    def _init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_quadrotor_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
        except:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/front_camera/camera_service", 1.0)
            self.get_front_camera = rospy.ServiceProxy("/front_camera/camera_service", CameraSensor)

            resp = self.get_front_camera()
            self.camera_height = resp.height
            self.camera_width = resp.width
            
        except:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/quadrotor/get_front_range", 0.5)
            self.get_front_dist_sensor = rospy.ServiceProxy("/quadrotor/get_front_range", LaserSensor)

            rospy.wait_for_service("/quadrotor/get_left_range", 0.5)
            self.get_left_dist_sensor = rospy.ServiceProxy("/quadrotor/get_left_range", LaserSensor)

            rospy.wait_for_service("/quadrotor/get_right_range", 0.5)
            self.get_right_dist_sensor = rospy.ServiceProxy("/quadrotor/get_right_range", LaserSensor)

            rospy.wait_for_service("/quadrotor/get_down_range", 0.5)
            self.get_down_dist_sensor = rospy.ServiceProxy("/quadrotor/get_down_range", LaserSensor)
        except:
            rospy.logerr("Service call failed: %s" % (e,))


    def _init_game_controller(self):

        try:
            rospy.wait_for_service("/robot_handler", 5.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = resp.message

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            rospy.logerr("Service call failed: %s" % (e,))

        # REMOVE TEMP
        try:
            rospy.wait_for_service("/user_used_yaw", 5.0)

            self.user_used_yaw = rospy.ServiceProxy('user_used_yaw', Trigger)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        # REMOVE TEMP
        try:
            rospy.wait_for_service("/user_used_move_y", 5.0)

            self.user_used_move_y = rospy.ServiceProxy('user_used_move_y', Trigger)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def _check_game_controller(self):
        resp = self.robot_handle()
        self.status = resp.message


    def move(self, speed_x, speed_y, speed_z):
        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "Stop":
                return

            self.last_twist_cmd.linear.x = speed_x
            self.last_twist_cmd.linear.y = speed_y
            self.last_twist_cmd.linear.z = speed_z

            self.quadrotor_cmd_vel.publish(self.last_twist_cmd)
            
            # REMOVE TEMP
            if speed_y != 0:
                self.user_used_move_y()

    def rotate(self, speed_yaw):
        if not self.status == "no_robot":
            if not self.status == "free_roam":
                self._check_game_controller()

            if self.status == "Stop":
                return

            self.last_twist_cmd.angular.z = speed_yaw

            self.quadrotor_cmd_vel.publish(self.last_twist_cmd)

            # REMOVE TEMP
            self.user_used_yaw()


    def get_latest_coordinates(self):
        pass

    def get_camera_data(self):
        resp = self.get_front_camera()

        image_rgb_data = []

        for i in range(self.camera_height * self.camera_width * 3):
            image_rgb_data.append(ord(resp.data[i]))

        return image_rgb_data

    @property
    def position(self):
        resp = self.get_quadrotor_state('quadrotor', '')
        p = resp.pose.position
        return Vector3D(p.x, p.y, p.z) # use Vector3D not Point for math operators

    @property
    def angle(self):
        resp = self.get_quadrotor_state('quadrotor', '')
        x = resp.pose.orientation.x
        y = resp.pose.orientation.y
        z = resp.pose.orientation.z
        w = resp.pose.orientation.w

        # TODO: What library should be used to extract euler yaw from quaternion?
        a = 2.0 * (w * z + x * y)
        b = 1.0 - 2.0 * (y * y + z * z)
        return math.atan2(a, b)

    @property
    def front(self):
        resp = self.get_front_dist_sensor()

        return resp.data

    @property
    def left(self):
        resp = self.get_left_dist_sensor()

        return resp.data

    @property
    def right(self):
        resp = self.get_right_dist_sensor()

        return resp.data

    @property
    def down(self):
        resp = self.get_down_dist_sensor()

        return resp.data

    def is_ok(self):
        if not rospy.is_shutdown():
            rospy.sleep(0.05)
            return True
        else:
            return False
